curl
====

Docker_ image to run `curl <https://curl.haxx.se>`_.
The image also includes `jq <https://stedolan.github.io/jq/>`_, a command-line JSON processor.

.. _Docker: https://www.docker.com
