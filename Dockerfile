FROM alpine:3.7

LABEL maintainer "benjamin.bertrand@esss.se"

ENV CURL_VERSION=7.60.0-r1 \
    JQ_VERSION=1.5-r5

RUN apk add --no-cache curl="${CURL_VERSION}" jq="${JQ_VERSION}"
RUN adduser -S csi

USER csi
